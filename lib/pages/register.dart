import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_laravel_api/api/register_api.dart';
import 'package:flutter_laravel_api/pages/login.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passController = new TextEditingController();

  bool _validasiName = false;
  bool _validasiEmail = false;
  bool _validasiPassword = false;

  String namaError = 'Tidak boleh kosong!';
  String emailError = 'Tidak boleh kosong!';
  String passError = 'Tidak boleh kosong!';

  _alertSukses() {
    final snackBar = SnackBar(
      backgroundColor: Colors.white,
      content: Row(
        children: [
          Icon(
            Icons.check_circle_sharp,
            color: Colors.green,
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            'Congratulation! Akun berhasil dibuat',
            style: TextStyle(color: Colors.black),
          ),
        ],
      ),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  _createAccount() async {
    var data = {
      'name': nameController.text,
      'email': emailController.text,
      'password': passController.text
    };

    var res = await RegisterApi().postData(data);
    var body = json.decode(res.body);
    print(jsonEncode(body));
    // print(body['message']['password']);
    if (body['status'] == 99) {
      setState(() {
        if (body['message']['name'] != null) {
          _validasiName = true;
          namaError = body['message']['name'].toString();
        }
        if (body['message']['email'] != null) {
          _validasiEmail = true;
          emailError = body['message']['email'].toString();
        }
        if (body['message']['password'] != null) {
          _validasiPassword = true;
          passError = body['message']['password'].toString();
        }
      });
    }

    if (body['status'] == 200) {
      setState(() {
        nameController.text = '';
        emailController.text = '';
        passController.text = '';
        _alertSukses();
      });
    }
  }

  nameListener(String nama) {
    nama.isEmpty ? _validasiName = true : _validasiName = false;
  }

  emailListener(String email) {
    email.isEmpty ? _validasiEmail = true : _validasiEmail = false;
  }

  passListener(String pass) {
    pass.isEmpty ? _validasiPassword = true : _validasiPassword = false;
  }

  bool validasiForm(String nama, email, password) {
    nama.isEmpty ? _validasiName = true : _validasiName = false;
    email.isEmpty ? _validasiEmail = true : _validasiEmail = false;
    password.isEmpty ? _validasiPassword = true : _validasiPassword = false;
    if (nama.isNotEmpty && email.isNotEmpty && password.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.redAccent,
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(10.0),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.78,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 3,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Image.asset(
                        'images/laravel.png',
                        width: 100.0,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Register with Laravel API",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        onChanged: (text) {
                          setState(() {
                            nameListener(text);
                          });
                        },
                        controller: nameController,
                        cursorColor: Colors.redAccent,
                        decoration: InputDecoration(
                          errorText: _validasiName ? namaError : null,
                          hintText: 'Name',
                          focusColor: Colors.redAccent,
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.redAccent,
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.redAccent,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        onChanged: (text) {
                          setState(() {
                            emailListener(text);
                          });
                        },
                        controller: emailController,
                        cursorColor: Colors.redAccent,
                        decoration: InputDecoration(
                          errorText: _validasiEmail ? emailError : null,
                          hintText: 'Email',
                          focusColor: Colors.redAccent,
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.redAccent,
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.redAccent,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        onChanged: (text) {
                          setState(() {
                            passListener(text);
                          });
                        },
                        controller: passController,
                        obscureText: true,
                        cursorColor: Colors.redAccent,
                        decoration: InputDecoration(
                          errorText: _validasiPassword ? passError : null,
                          hintText: 'Password',
                          focusColor: Colors.redAccent,
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.redAccent,
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.redAccent,
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                      Column(
                        children: [
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              fixedSize: Size(200, 40),
                              primary: Colors.redAccent,
                              elevation: 4,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            onPressed: () {
                              var result = false;
                              setState(() {
                                result = validasiForm(
                                  nameController.text,
                                  emailController.text,
                                  passController.text,
                                );
                                if (result) {
                                  _createAccount();
                                }
                              });
                            },
                            child: Text(
                              "Create Account",
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              fixedSize: Size(200, 40),
                              primary: Color(0xFFECF2F4),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Login(),
                                ),
                              );
                            },
                            child: Text(
                              "Back to Login",
                              style: TextStyle(
                                color: Colors.redAccent,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
