import 'dart:convert';
import 'package:http/http.dart ' as http;

class RegisterApi {
  final String url = 'http://127.0.0.1:8000/api/register';
  postData(data) async {
    return await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: _setHeaders(),
    );
  }

  _setHeaders() => {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      };
}
